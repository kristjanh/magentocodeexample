<?php
class Vigvam_Collague_Helper_Data extends Mage_Core_Helper_Abstract {

    private function getAbsPath() {
        return Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA).'/catalog/product';
    }

    public function getRatio($image_path) {
        $image = $this->getAbsPath().$image_path;
        try {
            $data = getimagesize($image);
            $x = floatval($data[0]);
            $y = floatval($data[1]);
        } catch(Exception $e) {
            return 0;
        }
        return $x / $y;
    }
    
    public function getCollagueSortedImages($images) {
        $images_with_ratio = array();
        $c=0;
        foreach ($images as $key=>$image) {
            $c++;
            if ($c == 1 || $c == 2) continue;
            $image->setRatio($this->getRatio($image->getFile()));
            $images_with_ratio[$key] = $image;
        }
        uasort($images_with_ratio, 'cmp');
        
        return $images_with_ratio;
    }
    
}


function cmp($a, $b) {
    $ar = $a->getRatio();
    $br = $b->getRatio();
    
    if ($ar == $br) {
        return 0;
    }
    
    return ($ar > $br) ? -1 : 1;
}