<?php
class Vigvam_CategoryAttributes_Helper_Data extends Mage_Core_Helper_Abstract {
    
    public function getProductImage($product, $type='thumbnail') {
        if (($product->getTypeInstance(true) instanceof Mage_Catalog_Model_Product_Type_Grouped)) {
            $_item = $product;
        } else {
            // Get parent
            $childId = $product->getId();
            $resource = $product->getResource();
            $read = Mage::getSingleton('core/resource')->getConnection('core_read');
            
            $groupedProductsTable = $resource->getTable('catalog/product_link');
            $groupedLinkTypeId = Mage_Catalog_Model_Product_Link::LINK_TYPE_GROUPED;
            
            $groupedSelect = $read->select()
                        ->from(array('g'=>$groupedProductsTable), 'g.product_id')
                        ->where("g.linked_product_id = ?", $childId)
                        ->where("link_type_id = ?", $groupedLinkTypeId);
                        
            $groupedIds = $read->fetchCol($groupedSelect);
            $superId = array_pop($groupedIds);
            
            $superProduct = Mage::getModel('catalog/product')->load($superId);
            return Mage::helper('catalog/image')->init($superProduct, 'thumbnail');
        }
        return Mage::helper('catalog/image')->init($_item, $type);
    }

    public function resize($image_name, $x, $y) {
        if (!$image_name || $image_name == "") {
            return false;
        }
    
        $media_path = Mage::getBaseDir('media').DS.'catalog'.DS.'category'.DS;
        $resizer_path = $media_path.'resized'.DS;
        
        $info = pathinfo($image_name);
        $resized_image_name = $info['filename'].'-'.$x.'x'.$y.'.'.$info['extension'];
        $resized_image = $resizer_path.$resized_image_name;
        
        $src_image = $media_path.$image_name;
        
        if (!file_exists($resizer_path))
            mkdir($resizer_path, 0777);
        
        // DO: add proper cache
        //if (!file_exists($resized_image)) {
            $imageObj = new Varien_Image($src_image);
            $imageObj->constrainOnly(true);
            $imageObj->keepAspectRatio(true);
            $imageObj->keepFrame(false);
            $imageObj->resize($x, $y);
            $imageObj->save($resized_image);
            
        //}
        
        return Mage::getBaseUrl('media')."catalog/category/resized/".$resized_image_name;
    }


}