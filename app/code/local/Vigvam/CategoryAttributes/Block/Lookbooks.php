<?php
class Vigvam_CategoryAttributes_Block_Lookbooks
    extends Mage_Core_Block_Abstract
    implements Mage_Widget_Block_Interface {

/*
    <ul>
        <li><a href="#">Casual</a></li>
        <li><a href="#">Cocktail</a></li>
        <li><a href="#">Work</a></li>
    </ul>
*/

    protected function _toHtml() {
        $categories = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToSort('name')
            ->addAttributeToFilter('listed_in_frontpage', 1);
    
        $html = '<ul>';
        foreach($categories as $c) {
            $html .= '<li><a href="'.$c->getUrl().'">'.$c->getName().'</a></li>';
        }
        $html .= '</ul>';
        return $html;
    }

}