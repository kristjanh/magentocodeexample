<?php

class Vigvam_CategoryAttributes_Block_Featured extends Mage_Core_Block_Template {
    
    public function addType($type='category') {
        $this->_type = $type;
    }

    public function getCategories() {
        $extra_cond = $this->_extra_cond;
        $categories = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToSort('name')
            ->addAttributeToFilter('featured', 1);
                
        
        // Cant really relay on this setting
        // Better would be checking the parent category template
        if ($this->_type == 'category') {
            $categories = $categories->addAttributeToFilter('custom_design', '');
        } elseif ($this->_type == 'look') {
            $categories = $categories->addAttributeToFilter('custom_design', 'beself_white/lookbook');
        }
        
        return $categories;
    }


}
