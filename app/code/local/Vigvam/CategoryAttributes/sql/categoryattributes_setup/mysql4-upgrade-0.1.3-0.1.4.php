<?php
$installer = $this;
$installer->startSetup();

$installer->removeAttribute('catalog_category', 'lookbook_picture');

$installer->endSetup();