<?php
class Vigvam_Recommendation_Helper_Data extends Mage_Core_Helper_Abstract
{

    function hasSizeData() {
        $session = Mage::getSingleton('customer/session');
        if ($session->isLoggedIn() === false) {
            return false;
        }
        $customer = $session->getCustomer();
        
        return $customer->getHeight() != "" && 
            $customer->getBust() != "" &&
            $customer->getWaist() != "" &&
            $customer->getHip() != "" &&
            $customer->getLeg() != "";
    }
    
    function hasPartialSizeData() {
        $session = Mage::getSingleton('customer/session');
        if ($session->isLoggedIn() === false) {
            return false;
        }
        $customer = $session->getCustomer();
        
        return $customer->getHeight() != "" ||
            $customer->getBust() != "" ||
            $customer->getWaist() != "" ||
            $customer->getHip() != "" ||
            $customer->getLeg() != "";
    }

    function isRecommended($product) {
        $session = Mage::getSingleton('customer/session');
        if ($session->isLoggedIn() === false) {
            return false;
        }
        $customer = $session->getCustomer();
        
        $fields = array(
            'height',
            'bust',
            'waist',
            'hip',
            'leg'
        );
        
        $_compared = false;
        
        foreach($fields as $field) {
            $method_name = "get".ucfirst($field);
            
            $_size = $customer->{$method_name}();
            list($min, $max) = explode('-', $product->{$method_name}(), 2);
            
            if (is_numeric($min) && is_numeric($max) && is_numeric($_size)) {
                $_compared = true; // Sets returned value if any comparable attribute was found
                if ($customer->getEnableImperial() === '1') {
                    $_size = (int)round(($_size * 0.393700787), 0);
                }
                if (in_array($_size, range($min, $max))) {
                    continue;
                } else {
                    // If any of comparable attributes does not match, return false
                    return false;
                }
            }
        }
        
        // If no comparison was made (no comparable attribute found), returns false
        return $_compared;
    }

}