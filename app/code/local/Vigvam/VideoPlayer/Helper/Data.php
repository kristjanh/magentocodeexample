<?php
class Vigvam_VideoPlayer_Helper_Data extends Mage_Core_Helper_Abstract {
    function getEncoder() {
        return new VideoEncoder($this);
    }
    
    function getDimensions() {
        return array(
            382,
            578
        );
    }
    
    function getEncoderPath() {
        return Mage::getBaseDir('media') . DS . 'catalog' . DS . 'category' . DS . 'encoded' . DS;
    }
    
    function getEncoderUrl() {
        return Mage::getBaseUrl('media').'catalog/category/encoded/';
    }
    
    function getUrl($video_file, $type='mp4') {
        $info = pathinfo($video_file);
        $video = $info['filename'].'.'.$type;
        
        if (file_exists($this->getEncoderPath().$video)) {
            return $this->getEncoderUrl().$video;
        } else {
            return false;
        }
    }
}

class VideoEncoder {
    function __construct($manager) {
        $this->manager = $manager;
        
        $dim = $this->manager->getDimensions();
        $this->size = $dim[0].'x'.$dim[1];
        
        $this->ffmpeg = '/home/kristjan/videoconvert/dist/';
        $this->ffmpeg_bin = "{$this->ffmpeg}bin/ffmpeg";
        putenv("LD_LIBRARY_PATH={$this->ffmpeg}lib/");
    }
    
    function runcli($command) {
        exec($command);
    }
    
    function encode($filename, $type) {
        $info = pathinfo($filename);
        $output = $this->manager->getEncoderPath().$info['filename'].'.'.$type;
        $input = $this->manager->getEncoderPath().'../'.$filename;

        switch($type) {
            case 'mp4':
                $this->runcli("{$this->ffmpeg_bin} -y -i {$input} -acodec libfaac -ab 96k -ar 44100 -b 345k -vcodec libx264 -vpre slower -vpre main -level 21 -refs 2 -bt 345k -threads 0 -s {$this->size} {$output}");
                break;
                
            case 'webm':
            case 'ogv':
                $this->runcli("{$this->ffmpeg_bin} -y -i {$input} -acodec libvorbis -ac 2 -ab 96k -ar 44100 -b 345k -s {$this->size} {$output}");
                break;
                
            default:
                throw new Exception('You need to specify a valid encoding. Look Vigvam_VideoPlayer_Helper_Data for details.');
                break;
        
        }
    }
}
