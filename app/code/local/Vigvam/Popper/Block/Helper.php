<?php

function sort_by_size($_a, $_b) {
    $a = $_a->getSize();
    $b = $_b->getSize();
    
    if ($a == $b) {
        return 0;
    }
    return ($a < $b) ? -1 : 1;    
}

class Vigvam_Popper_Block_Helper extends Mage_Core_Block_Template
{
    protected $_availableTemplates = array(
        'default' => 'review/helper/summary.phtml',
        'short'   => 'review/helper/summary_short.phtml'
    );
    
    protected $_priceBlock = array();
    protected $_priceBlockTypes = array();

    protected function _addProductAttributesAndPrices(Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection $collection)
    {
        return $collection
            ->addMinimalPrice()
            ->addFinalPrice()
            ->addTaxPercents()
            ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes())
            ->addUrlRewrite();
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        /* @var $block Mage_Catalog_Block_Product_Price_Template */
        $block = $this->getLayout()->getBlock('catalog_product_price_template');
        if ($block) {
            foreach ($block->getPriceBlockTypes() as $type => $priceBlock) {
                $this->addPriceBlockType($type, $priceBlock['block'], $priceBlock['template']);
            }
        }

        return $this;
    }

    public function addPriceBlockType($type, $block = '', $template = '')
    {
        if ($type) {
            $this->_priceBlockTypes[$type] = array(
                'block' => $block,
                'template' => $template
            );
        }
    }

    protected function _getPriceBlock($productTypeId)
    {
        if (!isset($this->_priceBlock[$productTypeId])) {
            $block = 'catalog/product_price';
            if (isset($this->_priceBlockTypes[$productTypeId])) {
                if ($this->_priceBlockTypes[$productTypeId]['block'] != '') {
                    $block = $this->_priceBlockTypes[$productTypeId]['block'];
                }
            }
            $this->_priceBlock[$productTypeId] = $this->getLayout()->createBlock($block);
        }
        return $this->_priceBlock[$productTypeId];
    }    
    
    public function getPriceHtml($product, $displayMinimalPrice = false, $idSuffix='')
    {
        // Reload product with price and stuff
        $new_collection = $product->getCollection();
        $this->_addProductAttributesAndPrices($new_collection);
        $product = $new_collection->getItemById($product->getId());
        
        return $this->_getPriceBlock($product->getTypeId())
            ->setTemplate('catalog/product/price.phtml')
            ->setProduct($product)
            ->setDisplayMinimalPrice($displayMinimalPrice)
            ->setIdSuffix($idSuffix)
            ->setUseLinkForAsLowAs(true)
            ->toHtml();
    }    
    
    public function getHtml($product, $list_instance, $idx='') {
        if (!($product->getTypeInstance(true) instanceof Mage_Catalog_Model_Product_Type_Grouped)) {
            return '';
        }
        $this->setTemplate('popper/buy.phtml');
        $related = $product->getTypeInstance(true)->getAssociatedProducts($product);
        uasort($related, 'sort_by_size');
        $this->setRelatedProducts($related);
        $this->setProduct($product);
        $this->setListInstance($list_instance);
        $this->setIdx($idx);
        return $this->toHtml();
    }
    
    public function getSummaryHtml($product, $templateType, $displayIfNoReviews)
    {
        // pick template among available
        if (empty($this->_availableTemplates[$templateType])) {
            $templateType = 'default';
        }
        $this->setTemplate($this->_availableTemplates[$templateType]);

        $this->setDisplayIfEmpty($displayIfNoReviews);

        if (!$product->getRatingSummary()) {
            Mage::getModel('review/review')
               ->getEntitySummary($product, Mage::app()->getStore()->getId());
        }
        $this->setProduct($product);

        return $this->toHtml();
    }

    public function getRatingSummary()
    {
        return $this->getProduct()->getRatingSummary()->getRatingSummary();
    }

    public function getReviewsCount()
    {
        return $this->getProduct()->getRatingSummary()->getReviewsCount();
    }

    public function getReviewsUrl()
    {
        return Mage::getUrl('review/product/list', array(
           'id'        => $this->getProduct()->getId(),
           'category'  => $this->getProduct()->getCategoryId()
        ));
    }

    /**
     * Add an available template by type
     *
     * It should be called before getSummaryHtml()
     *
     * @param string $type
     * @param string $template
     */
    public function addTemplate($type, $template)
    {
        $this->_availableTemplates[$type] = $template;
    }
}
