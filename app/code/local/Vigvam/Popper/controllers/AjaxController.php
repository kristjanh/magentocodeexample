<?php
class Vigvam_Popper_AjaxController extends Mage_Core_Controller_Front_Action {
    
    public function addToCartAction() {
        try {
            $product_ids = (!isset($_GET['id'])) ? array() : $_GET['id'];
            
            $session = Mage::getSingleton('core/session', array('name'=>'frontend'));
            $cart = Mage::helper('checkout/cart')->getCart();
            
            foreach ($product_ids as $product_id) {
                $product = Mage::getModel('catalog/product')->load($product_id);
                $cart->addProduct($product, 1);
                $session->setLastAddedProductId($product->getId());
            }
            
            if (count($product_ids) > 0) {
                $session->setCartWasUpdated(true);
                $cart->save();
            }
            
            echo json_encode(array('result' => 'success', 'products' => $product_ids));
            
        } catch (Exception $e) {
            echo json_encode(array('result' => 'error', 'message' => $e->getMessage()));
            
        }            
    
    }
    
    public function toggleWishlistAction() {
        $helper = Mage::helper('popper');
        $productId = (int) $this->getRequest()->getParam('product');
        if (!$productId) {
            echo json_encode(array(
                'result' => 'error',
                'message' => $this->__('Missing product ID.')
            ));
            return;
        }
        
        if ($helper->inWishlist($productId)) {
            return $this->removeFromWishlistAction();
        } else {
            return $this->addToWishlistAction();
        }
    }
    
    public function removeFromWishlistAction() {
        if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
            echo json_encode(array('result' => 'redirect', 'url' => Mage::getUrl('customer/account/login')));
            return;
        }
    
        try {
            $session = Mage::getSingleton('customer/session');
            $wishlist = Mage::getModel('wishlist/wishlist')->loadByCustomer($session->getCustomer(), true);
        
            $productId = (int) $this->getRequest()->getParam('product');
            if (!$productId) {
                echo json_encode(array(
                    'result' => 'error',
                    'message' => $this->__('Missing product ID.')
                ));
                return;
            }
            
            $sharedStores = array(Mage::app()->getStore()->getStoreId());
            $item = Mage::getModel('wishlist/item')->loadByProductWishlist($wishlist->getId(), $productId, $sharedStores);
            $product = $item->getProduct();
            
            if($item->getWishlistId() == $wishlist->getId()) {
                $item->delete();
                $wishlist->save();
                Mage::helper('wishlist')->calculate();
            } else {
                throw new Exception('Item could not be removed from wishlist.');
            }
            
            $message = $this->__('%1$s has been removed from your wishlist.', $product->getName());
            echo json_encode(array(
                'result' => 'success',
                'message' => $message
            ));
            return;
            
        } catch (Exception $e) {
            echo json_encode(array(
                'result' => 'error',
                'message' => $e->getMessage()
            ));
        }
    }
    
    public function addToWishlistAction() {
        if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
            echo json_encode(array('result' => 'redirect', 'url' => Mage::getUrl('customer/account/login')));
            return;
        }
    
        try {
            $session = Mage::getSingleton('customer/session');
            $wishlist = Mage::getModel('wishlist/wishlist')->loadByCustomer($session->getCustomer(), true);
        
            $productId = (int) $this->getRequest()->getParam('product');
            if (!$productId) {
                echo json_encode(array(
                    'result' => 'error',
                    'message' => $this->__('Missing product ID.')
                ));
                return;
            }
            
            $product = Mage::getModel('catalog/product')->load($productId);
            if (!$product->getId() || !$product->isVisibleInCatalog()) {
                echo json_encode(array(
                    'result' => 'error',
                    'message' => $this->__('Cannot specify product.')
                ));            
                return;
            }        
            
            $wishlist->addNewItem($product->getId());
            $wishlist->save();
            
            Mage::dispatchEvent('wishlist_add_product', array('wishlist'=>$wishlist, 'product'=>$product));
            Mage::helper('wishlist')->calculate();
            
            $message = $this->__('%1$s has been added to your wishlist.', $product->getName());
            echo json_encode(array(
                'result' => 'success',
                'message' => $message
            ));
                        
        } catch (Exception $e) {
            echo json_encode(array(
                'result' => 'error',
                'message' => $e->getMessage()
            ));
        }
    
    }
}