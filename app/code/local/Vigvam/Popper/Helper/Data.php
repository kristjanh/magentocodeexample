<?php
class Vigvam_Popper_Helper_Data extends Mage_Core_Helper_Abstract {
    private $_product_ids = false;
    
    public function inWishlist($product) {
        if (is_numeric($product)) {
            $productId = $product;
        } else {
            $productId = $product->getId();
        }
        $wishlistIds = $this->getWishlistProductIds();
        return in_array($productId, $wishlistIds);
    }
    
    private function getWishlistProductIds() {
        if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
            return array();
        }
        
        if (!$this->_product_ids) {
            $this->_product_ids = array();
            $session = Mage::getSingleton('customer/session');
            $wishlist = Mage::getModel('wishlist/wishlist')->loadByCustomer($session->getCustomer(), true);
            foreach($wishlist->getProductCollection() as $p) {
                array_push($this->_product_ids, $p->getId());
            }
            return $this->_product_ids;
        }
        
        return $this->_product_ids;
    }


}