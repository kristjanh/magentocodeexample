<?php
class Vigvam_AjaxCart_CartController extends Mage_Core_Controller_Front_Action {
    
    private function getSidebar() {
        if (!$this->_sidebar) {
            $this->_sidebar = $this->getLayout()->createBlock('checkout/cart_sidebar')
                ->addItemRender($type="simple", $block="checkout/cart_item_renderer", $template="checkout/cart/sidebar/default.phtml")
                ->addItemRender($type="grouped", $block="checkout/cart_item_renderer_grouped", $template="checkout/cart/sidebar/default.phtml")
                ->addItemRender($type="configurable", $block="checkout/cart_item_renderer_configurable", $template="checkout/cart/sidebar/default.phtml");
        }
        return $this->_sidebar;
    }
    
    public function itemsAction() {
        echo $this->getLayout()->createBlock('ajaxcart/sidebar')->getHtml($this->getSidebar());
    }
    
    public function priceAction() {
        echo $this->getLayout()->createBlock('ajaxcart/sidebar_price')->getHtml($this->getSidebar());
    }

    public function deleteAction() {
        $id = (int) $this->getRequest()->getParam('id');
        if ($id) {
            try {
                Mage::getSingleton('checkout/cart')->removeItem($id)->save();
            } catch (Exception $e) {
                Mage::getSingleton('checkout/session')->addError($this->__('Cannot remove the item.'));
            }
        }
        return;
    }

}