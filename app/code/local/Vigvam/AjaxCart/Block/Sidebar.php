<?php
class Vigvam_AjaxCart_Block_Sidebar extends Mage_Core_Block_Template {
    
    public function renderItemHtml($item) {
        return $this->_block->getItemHtml($item);
    }
    
    public function getHtml($block) {
        $this->_block = $block;
        $this->setRecentItems($block->getItems());
/*         $this->setRecentItems($block->getRecentItems()); */
        $this->setTemplate('ajaxcart/items.phtml');
        return $this->toHtml();
    }

}