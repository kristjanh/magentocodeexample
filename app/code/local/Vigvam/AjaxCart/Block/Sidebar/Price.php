<?php
class Vigvam_AjaxCart_Block_Sidebar_Price extends Mage_Core_Block_Template {
    
    public function getHtml($block) {
        $this->_block = $block;
        
        $this->setSubtotalInclTax($block->getSubtotalInclTax());
        $this->setSubtotal($block->getSubtotal());
        
        $this->setTemplate('ajaxcart/price.phtml');
        return $this->toHtml();
    }

}