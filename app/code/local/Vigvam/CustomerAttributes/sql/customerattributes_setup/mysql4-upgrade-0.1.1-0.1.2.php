<?php
$installer = $this;
/* @var $installer Vigvam_CustomerAttributes_Model_Entity_Setup */
$installer->startSetup();

$installer->addAttribute('customer', 'height', array(
	'label'		=> 'Height',
	'type'		=> 'varchar',
	'input'		=> 'text',
	'visible'	=> true,
	'required'	=> false,
	'position'	=> 1,
	'user_defined' => true
));

$installer->addAttribute('customer', 'leg', array(
	'label'		=> 'Leg',
	'type'		=> 'varchar',
	'input'		=> 'text',
	'visible'	=> true,
	'required'	=> false,
	'position'	=> 5,
	'user_defined' => true
));

$installer->endSetup();