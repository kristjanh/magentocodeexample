<?php
$installer = $this;
/* @var $installer Vigvam_CustomerAttributes_Model_Entity_Setup */
$installer->startSetup();

$installer->addAttribute('customer', 'size', array(
	'label'		=> 'Size',
	'type'		=> 'varchar',
	'input'		=> 'text',
	'visible'	=> true,
	'required'	=> false,
	'position'	=> 1,
));

$installer->addAttribute('customer', 'bra', array(
	'label'		=> 'Bra size',
	'type'		=> 'varchar',
	'input'		=> 'text',
	'visible'	=> true,
	'required'	=> false,
	'position'	=> 2,
));

$installer->addAttribute('customer', 'bust', array(
	'label'		=> 'Bust size',
	'type'		=> 'varchar',
	'input'		=> 'text',
	'visible'	=> true,
	'required'	=> false,
	'position'	=> 3,
));

$installer->addAttribute('customer', 'waist', array(
	'label'		=> 'Waist size',
	'type'		=> 'varchar',
	'input'		=> 'text',
	'visible'	=> true,
	'required'	=> false,
	'position'	=> 4,
));

$installer->addAttribute('customer', 'hip', array(
	'label'		=> 'Hip size',
	'type'		=> 'varchar',
	'input'		=> 'text',
	'visible'	=> true,
	'required'	=> false,
	'position'	=> 5,
));

$installer->endSetup();