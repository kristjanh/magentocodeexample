<?php
/**
 * Customer resource setup model
 *
 * @category   Mage
 * @package    Mage_Customer
 */
class Vigvam_CustomerAttributes_Model_Entity_Setup extends Mage_Customer_Model_Entity_Setup
{
    public function getDefaultEntities()
    {
    
    
        //
        // PS: ADD FIELD DEF TO customer_form_attribute TABLE
        //
        
        
        
        $defaultEntities = parent::getDefaultEntities();
        
        $defaultEntities['customer']['attributes']['size'] = array(
        	'label'		=> 'Size',
        	'visible'	=> true,
        	'required'	=> false,
            'sort_order'    => 90,
        );

        $defaultEntities['customer']['attributes']['height'] = array(
        	'label'		=> 'Height',
        	'visible'	=> true,
        	'required'	=> false,
            'sort_order'    => 100,
        );
        
        $defaultEntities['customer']['attributes']['bust'] = array(
        	'label'		=> 'Bust',
        	'visible'	=> true,
        	'required'	=> false,
            'sort_order'    => 130,
        );
                
/*
        $defaultEntities['customer']['attributes']['bra'] = array(
        	'label'		=> 'Bra size',
        	'visible'	=> true,
        	'required'	=> false,
            'sort_order'    => 100,
        );
*/

        $defaultEntities['customer']['attributes']['waist'] = array(
        	'label'		=> 'Waist',
        	'visible'	=> true,
        	'required'	=> false,
            'sort_order'    => 150,
        );

        $defaultEntities['customer']['attributes']['hip'] = array(
        	'label'		=> 'Hip size',
        	'visible'	=> true,
        	'required'	=> false,
            'sort_order'    => 120,
        );

        $defaultEntities['customer']['attributes']['leg'] = array(
        	'label'		=> 'Leg',
        	'visible'	=> true,
        	'required'	=> false,
            'sort_order'    => 200,
        );

        $defaultEntities['customer']['attributes']['enable_imperial'] = array(
        	'label'		=> 'Use imperial units',
        	'visible'	=> true,
        	'required'	=> false,
            'sort_order'    => 140,
        );

        
        return $defaultEntities;
    }
}
