<?php
class Vigvam_GlobalBlocks_Block_Title extends Mage_Core_Block_Template {
    
    public function getHtml() {
        $this->setTemplate('global_blocks/title.phtml');
        return $this->toHtml();
    }
    
    public function setTitle($title='') {
        $this->title = $title;
    }
    
    public function setType($type='category') {
        switch($type) {
            case 'category':
                $currentCategory = Mage::registry('current_category');
                $this->title = $currentCategory->getName();
                break;
                
            case 'static':
                break;
        }
    }

}
